package com.umitkirtil;

import com.umitkirtil.aspect.AspectConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(value = {AspectConfig.class})
public class LogbacklogstashApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogbacklogstashApplication.class, args);
    }
}
