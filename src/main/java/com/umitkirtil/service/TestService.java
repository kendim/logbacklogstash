package com.umitkirtil.service;

import org.springframework.stereotype.Service;

/**
 * Created by UmitKIRTIL on 26.01.2017.
 */
@Service
public class TestService {

    public String testYazdir() {
        return "Test Servisin içi";
    }
}
