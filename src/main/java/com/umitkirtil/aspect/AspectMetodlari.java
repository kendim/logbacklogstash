package com.umitkirtil.aspect;

import ch.qos.logback.classic.Level;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by UmitKIRTIL on 26.01.2017.
 */

@Aspect
@Component
public class AspectMetodlari {

    Logger logger = LoggerFactory.getLogger("Example App");

    @Before("execution(* com.umitkirtil.service.TestService.testYazdir())")
    public void testYazdirOncesindeLog() {
        System.out.println("AspectMetodlari içi - Test Yazdır Öncesi");
        logger.info("Aspect - info Log");
        logger.debug("Aspect - debug Log");

        logger.trace("Trace Message!");
        logger.debug("Debug Message!");
        logger.info("Info Message!");
        logger.warn("Warn Message!");
        logger.error("Error Message!");
    }

}
