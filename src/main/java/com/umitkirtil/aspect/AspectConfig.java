package com.umitkirtil.aspect;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created by UmitKIRTIL on 26.01.2017.
 */
@Configuration
@EnableAspectJAutoProxy
public class AspectConfig {
}
