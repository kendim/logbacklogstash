# Logback ve LogStash Uygulaması #

Oluşturduğumuz logları logstash'a göndermemizi sağlayan basit bir logback ayarı içeren projede  ayrıca AOP'de kullanılmakta.

### Nasıl Kurarım ? ###

* Öncelikle çalışan bir logstash sunucusuna ihtiyacımız var. Bunun için Docker kullanabilirsiniz. Uzun yolunu ben anlatmayacağım.
#####docker run -it --rm -p 6001:6001 -p 5001:5001 -p 6000:6000 logstash -e 'input { stdin{}  http { port => 5001 type => syslog } tcp { port => 6001 type => syslog }  udp { port => 6000 type => syslog } } output { stdout{} } ' --debug --verbose
(Yukarıdaki komut sizin için gerekli dosyaları indirerek logstash'ınızı verdiğiniz ayarlar ile çalıştıracaktır.)

* Gerekli Kütüphaneler
######AOP  , Logback (maven artifact id : logback-classic)


### Yazının Sahibi ###

* Ümit KIRTIL.

Kullanılan Kaynaklar : 
* https://logback.qos.ch/manual/configuration.html
* https://balamaci.ro/java-app-monitoring-with-elk-logstash/
* https://hub.docker.com/_/logstash/